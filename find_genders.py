import csv

import pgd
from finglish import f2p

with open('first_names.csv', encoding="utf-8") as csvfile:
    reader = csv.reader(csvfile, dialect=csv.excel)
    with open('first_names_new.csv', 'w', encoding="utf-8") as csvfile_new:
        writer = csv.writer(csvfile_new)
        for row in reader:
            firstName = str(row[1]).split()[0]
            farsiName = f2p(firstName)
            row[3] = pgd.detectGender(farsiName)
            row[4] = firstName
            row[5] = farsiName
            writer.writerow(row)
