from finglish import f2p
import pandas as pd
from openpyxl import load_workbook
import pgd

df = pd.read_excel ('first_names_242100_242200.xlsx') 
firstNameResults = []
results = []
genderRresults = []
for name in df.FIRST_NAME:
    firstName = str(name).split()[0]
    farsiName = f2p(firstName)
    firstNameResults.append(firstName)
    results.append(farsiName)
    genderRresults.append(pgd.detectGender(farsiName))


df_new = pd.DataFrame({'firstName': firstNameResults, 'farsiName': results, 'Gender': genderRresults})
wb = load_workbook('first_names_242100_242200.xlsx')

ws = wb['AOI']

for index, row in df_new.iterrows():
    cell1 = 'E%d'  % (index + 2)
    ws[cell1] = row[0]

    cell2 = 'D%d'  % (index + 2)
    ws[cell2] = row[2]

    cell1 = 'F%d'  % (index + 2)
    ws[cell1] = row[1]

wb.save('first_names_242100_242200.xlsx')
